export interface IMessageService {

    addSuccess(title: string, message: string): void;
    addInfo(title: string, message: string): void;
    addError(title: string, message: string): void;
    addWarn(title: string, message: string): void;

    clear(): void;
}

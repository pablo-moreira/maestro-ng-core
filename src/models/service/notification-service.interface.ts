export interface NotificationService {

  showInfo(title: string, message: string): void;
  showError(title: string, message: string): void;
  showWarn(title: string, message: string): void;

}

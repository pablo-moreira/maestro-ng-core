export class PagedQueryResult<E> {
    public entities: E[];
    public count: number;
}

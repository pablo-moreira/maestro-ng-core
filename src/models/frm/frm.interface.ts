import { EntityService } from "../service/entity.service";

export interface Frm<E> {
  save(): Promise<E>;
  startInsert(entity?: E): void;
  startUpdate(entity: E): void;
  cancel(): void;
  getEntity(): E;
  getService(): EntityService<E, any>;
}

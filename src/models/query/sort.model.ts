export class Sort {

    attribute: string;
    order: string;

    static asc(attribute: string) {
        return {
            attribute: attribute,
            order: 'ASC'
        };
    }

    static desc(attribute: string) {
        return {
            attribute: attribute,
            order: 'DESC'
        };
    }
}

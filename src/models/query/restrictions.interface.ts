import { Restriction } from './restriction.model';

export interface IRestrictions {
  getEnables(): Array<Restriction<any>>;
  attr(attribute: string): Restriction<any>;
  clear(): void;
}

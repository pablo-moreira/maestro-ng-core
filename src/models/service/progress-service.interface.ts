import { Observable } from 'rxjs';

export interface IProgressService {
  hide(): void;
  showModal(): void;
  showModeless(): void;
}

import { Frm } from './frm.interface';
import { FrmAssociationOneToManyConfigs } from './frm-association-one-to-many.component';
import { IMessageService } from '../service/message-service.interface';
import { IConfirmationService } from '../service/confirmation-service.interface';

export abstract class DlgFrmAssociationOneToMany<E, EF extends Frm<E>, A, AF extends Frm<A>> {

    public visible = false;
    private configs: FrmAssociationOneToManyConfigs;
    private associationSelected: A;

    constructor(
        protected frmEntity: EF,
        protected frmAssociation: AF,
        protected messageService: IMessageService,
        protected confirmationService: IConfirmationService
    ) {
        this.configs = this.initConfigs();
    }

    public startInsert(): void {
        this.startInsertAction();
    }

    public startInsertAction(): A {

        this.frmAssociation.startInsert();

        const association = this.frmAssociation.getEntity();

        this.connect(association);

        this.visible = true;

        return association;
    }

    public startUpdate(association: A): void {
        this.frmAssociation.startUpdate(association);
        this.visible = true;
    }

    public updateEntityAssociations(associations: A[]): void {

        const entity = this.frmEntity.getEntity();

        (entity as any)[this.configs.oneToManyAttributeName] = [...associations];
    }

    public getAssociations(entity: any): A[] {

        let associations = entity[this.configs.oneToManyAttributeName];

        if (associations === undefined) {
            associations = [];
        }

        return associations;
    }

    public connect(association: A) {

        const entity = this.frmEntity.getEntity();

        (association as any)[this.configs.manyToOneAttributeName] = entity;
    }

    public abstract initConfigs(): FrmAssociationOneToManyConfigs;

    public startDelete(associationSelected: A): void {

        this.associationSelected = associationSelected;

        this.confirmationService.confirm({
            title: this.configs.deleteTitle,
            message: this.configs.deleteMessage,
            accept: () => this.delete(),
            reject: () => this.cancelDelete()
        });
    }

    public beforeDelete(association: A, entity: E): void {
        // Change
    }

    public afterDelete(association: A, entity: E): void {
        // Change
    }

    public delete(): void {

        const association = this.associationSelected;
        const entity = this.frmEntity.getEntity();

        this.beforeDelete(association, entity);

        if (this.frmAssociation.getService().getId(association) !== null) {
            this.frmAssociation.getService().delete(association)
                .then(() => {
                    this.deleteAction(association, entity);
                })
                .catch(response => {
                    this.messageService.addError('Erro', response.error);
                });
        }
        else {
            this.deleteAction(association, entity);
        }
    }

    public cancelDelete(): void {
        this.associationSelected = undefined;
    }

    private deleteAction(association: A, entity: E): void {

        const associations = this.getAssociations(entity);

        const index = associations.indexOf(association);

        if (index > -1) {
            associations.splice(index, 1);
            this.updateEntityAssociations(associations);
        }

        this.afterDelete(association, entity);

        this.cancelDelete();
    }

    public cancel(): void {
        this.frmAssociation.cancel();
        this.visible = false;
    }

    public save(): void {

        const isInserting = this.frmAssociation.getService().isTransient(this.frmAssociation.getEntity());

        this.frmAssociation.save()
            .then(associationSaved => {

                const entity = this.frmEntity.getEntity();
                const associations = this.getAssociations(entity);

                if (isInserting) {
                    this.updateEntityAssociations([...associations, associationSaved]);
                }
                else {

                    const getId = this.frmAssociation.getService().getId;
                    const associationId = getId(associationSaved);

                    for (let i = 0; i < associations.length; i++) {
                        if (getId(associations[i]) === associationId) {
                            associations[i] = associationSaved;
                        }
                    }

                    this.updateEntityAssociations([... associations]);
                }

                this.frmAssociation.cancel();

                this.visible = false;
            });
    }
}